/**
 * Created by crystall on 02.11.2017.
 */
public class Operand {
    private String name;
    private boolean lock;
    public int availabilityIndex; //такт, с которого доступен операнд для следующих команд
    public int value;
    private AddressType addressType;

    Operand(){
        lock = false;
        addressType = AddressType.DEFAULT;
        availabilityIndex = 0;
        value = 1;
    }

    Operand(String _name){
        this();
        name = _name;
        this.setAddressType();
    }

    public boolean isLocked(){
        return lock == true;
    }

    public void setLock(boolean _lock){
        lock = _lock;
    }

    public String getName() { return name; }

    public String toString() {
        return name;
    }

    public AddressType getAddressType() { return addressType; }

    public boolean equals(String _nameOperand){
        return this.name.equals(_nameOperand);
    }

    public void setAddressType(AddressType _addressType){
        addressType = _addressType;
    }

    private void setAddressType() {
        if (isMemoryAddress()){
            addressType = AddressType.INTERNAL_MEM;
        }
        else {
            addressType = AddressType.CONSTANT;
        }
    }

    private boolean isDigit(){
        try{
            Integer.parseInt(name);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    private boolean isMemoryAddress(){
        return  name.contains("[") && name.contains("]");
    }

    public boolean isConstant(){
        return addressType == AddressType.CONSTANT;
    }

    public boolean isOffset() { return name.length() > 6 && name.substring(0,6).equalsIgnoreCase("offset"); }

    public boolean isExternalMemory() { return addressType == AddressType.EXTERNAL_MEM; }

    public boolean isInternalMemory() { return addressType == AddressType.INTERNAL_MEM; }
}
