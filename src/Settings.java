import javax.swing.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Settings extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel instructionsLabel;
    private JTextArea textArea1;
    private JComboBox variantNum;
    private JCheckBox reuseProhibitedCB;
    private JCheckBox sameTaktCB;
    private JCheckBox splitFirstCB;
    private JCheckBox duplicateStageCB;
    private JCheckBox addPredictCB;

    private int result;

    public Settings(Parameters param) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        getAllVariants();

        variantNum.setSelectedIndex(param==null ? 0 : param.varNum-1);

        textArea1.setText(param==null ? loadVariant(variantNum.getSelectedItem().toString() + ".txt") : param.ins);

        if (param != null){
            reuseProhibitedCB.setSelected(param.reuseProhibited);
            sameTaktCB.setSelected(param.sameTakt);
            splitFirstCB.setSelected(param.splitFirst);
            duplicateStageCB.setSelected(param.duplicateStage);
            addPredictCB.setSelected(param.addPredict);
        }

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        variantNum.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //textArea1.setText(Variants.variants[variantNum.getSelectedIndex()]);
                textArea1.setText(loadVariant(variantNum.getSelectedItem().toString() + ".txt"));
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        result = 1;
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        result = 2;
        dispose();
    }

    private void getAllVariants(){
        File dir = new File("./variants");
        for (File item: dir.listFiles()){
            variantNum.addItem(item.getName().replace(".txt", ""));
        }
    }

    private String loadVariant(String fileName){
        try{
            File file = new File("./variants/" + fileName);
            FileReader fread = new FileReader(file);
            BufferedReader bin = new BufferedReader(fread);
            String line;
            String result = "";
            while ((line = bin.readLine()) != null){
                result += line + "\n";
                //System.out.print(line);
            }
            fread.close();
            return result;
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public Parameters getParameters(){
        Parameters param = new Parameters();
        param.reuseProhibited = reuseProhibitedCB.isSelected();
        param.addPredict = addPredictCB.isSelected();
        param.duplicateStage = duplicateStageCB.isSelected();
        param.sameTakt = sameTaktCB.isSelected();
        param.splitFirst = splitFirstCB.isSelected();
        param.ins = textArea1.getText();
        param.convNum = 1;
        param.varNum = variantNum.getSelectedIndex()+1;
        return param;
    }

    public int showDialog(){
        setVisible(true);
        return result;
    }
}
