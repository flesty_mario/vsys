import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by crystall on 04.11.2017.
 */
public class ProcessConveyorMain extends JFrame{
    private JPanel rootPanel;
    private JPanel TopPanel;
    private JPanel MainPanel;
    private JButton btn_launch;
    private JButton btn_settings;
    private JButton btn_stat;
    private JButton btn_close;
    private JButton btn_save;
    private JButton btn_editor;
    private JButton btn_help;
    private ArrayList<JTable> conv;
    private JTable cnv;
    private Object[] headers;
    private Object[][] data;
    private DefaultTableModel tableModel;

    private Parameters param;
    private Agent agt;

    private boolean isAdministrator;

    public ProcessConveyorMain(boolean _isAdministrator){

        isAdministrator = _isAdministrator;

        ActionListener btnSettingsClickListener = new onClickBtnSettings();
        btn_settings.addActionListener(btnSettingsClickListener);

        ActionListener btnLaunchClickListener = new onClickBtnLaunch();
        btn_launch.addActionListener(btnLaunchClickListener);

        ActionListener btnStatisticClickListener = new onClickBtnStat();
        btn_stat.addActionListener(btnStatisticClickListener);

        ActionListener btnCloseClickListener = new onClickBtnClose();
        btn_close.addActionListener(btnCloseClickListener);

        ActionListener btnSaveClickListener = new onClickBtnSave();
        btn_save.addActionListener(btnSaveClickListener);

        ActionListener btnEditClickListener = new onClickBtnEditor();
        btn_editor.addActionListener(btnEditClickListener);

        ActionListener btnHelpClickListener = new onClickBtnHelp();
        btn_help.addActionListener(btnHelpClickListener);

        btn_editor.setVisible(isAdministrator);

        agt = null;

        param = null;

        setContentPane(rootPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(1500, 650);
        setVisible(true);
    }

    private void startProcessConveyor(){
        try {

            if (agt == null || agt.isParametersSetted() == false){
                JOptionPane.showMessageDialog(null, "Сначала нужно задать параметры (кнопка Настройки).");
                return;
            }

            if (agt.isTaskDone()) return;

            agt.startProcessCommand();

            MainPanel.removeAll();
            MainPanel.setLayout(new GridLayout());

            int conveyorNum = agt.getNumOfConveyors();
            int stageNum = agt.getNumOfBlocks();

            setHeaders(stageNum, conveyorNum);

            JFrame jfrm = new JFrame("Conveyor");
            jfrm.getContentPane().setLayout(new FlowLayout());
            jfrm.setSize(1400, 600);

            //System.out.println("processing check");
            //data = agt.fetchProcessingResult();
            tableModel = agt.fetchResults(headers);
            //System.out.println("fetching check");

            cnv = new JTable();
            cnv.setPreferredScrollableViewportSize(new Dimension(1400, 600));
            JScrollPane jscrlp = new JScrollPane(cnv);
            cnv.setModel(tableModel);
            cnv.getColumn(headers[0]).setMinWidth(60);
            cnv.getColumn(headers[0]).setMaxWidth(60);
            cnv.getColumn(headers[0]).setPreferredWidth(60);
            cnv.getColumn(headers[0]).setResizable(false);
            cnv.getColumn(headers[0]).setCellRenderer(new DefaultTableCellRenderer(){
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
                    setText(value.toString());
                    setBackground(Color.lightGray);
                    return this;
                }
            });
            cnv.setRowSelectionAllowed(false);
            cnv.setEnabled(false);
            MainPanel.add(jscrlp);
            MainPanel.updateUI();

            //param = null;
        }
        catch (Exception ex){
            System.out.println(ex.getMessage() + "\n " + ex.getCause() + "\n");
            ex.printStackTrace();
        }
    }

    private void saveProcessResult(){
        try {
            if (agt == null || !agt.isTaskDone()){
                JOptionPane.showMessageDialog(null, "Сначала нужно выполнить программу");
                return;
            }
            String settings = "1" + agt.hasProhibitedReuse().toLowerCase() + "2" + agt.hasSameLength().toLowerCase() + "3" + agt.hasAdditionalStages().toLowerCase() + "4" + agt.hasDuplicateStage().toLowerCase() + "5" + agt.hasPrediction().toLowerCase();
            String fileName = "./results/" + "V" + agt.getVariant() + "_" + settings + ".txt";
            File file = new File(fileName);
            if (file.exists()){
                Object[] options = {"да", "нет"};
                int n = JOptionPane.showOptionDialog(null, "Для данных настроек уже были сохранены результаты другой обработки. Перезаписать их?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                //System.out.println(n);
                if (n == 0) {
                    file.delete();
                    file.createNewFile();
                    FileWriter fout = new FileWriter(file, true);
                    fout.write("" + agt.getVariant());
                    fout.write("\r\n");
                    fout.write("" + agt.getNumOfBlocks());
                    fout.write("\r\n");
                    fout.write("" + agt.getConveyors().get(0).size());
                    fout.write("\r\n");
                    fout.write("" + agt.hasAdditionalStages());
                    fout.write("\r\n");
                    fout.write("" + agt.hasDuplicateStage());
                    fout.write("\r\n");
                    fout.write("" + agt.hasProhibitedReuse());
                    fout.write("\r\n");
                    fout.write("" + agt.hasPrediction());
                    fout.write("\r\n");
                    fout.write("" + agt.hasSameLength());
                    fout.write("\r\n");
                    fout.close();
                }
            }
            else {
                if (file.createNewFile()) {
                    FileWriter fout = new FileWriter(file, true);
                    fout.write("" + agt.getVariant());
                    fout.write("\r\n");
                    fout.write("" + agt.getNumOfBlocks());
                    fout.write("\r\n");
                    fout.write("" + agt.getConveyors().get(0).size());
                    fout.write("\r\n");
                    fout.write("" + agt.hasAdditionalStages());
                    fout.write("\r\n");
                    fout.write("" + agt.hasDuplicateStage());
                    fout.write("\r\n");
                    fout.write("" + agt.hasProhibitedReuse());
                    fout.write("\r\n");
                    fout.write("" + agt.hasPrediction());
                    fout.write("\r\n");
                    fout.write("" + agt.hasSameLength());
                    fout.write("\r\n");
                    fout.close();
                } else {
                    JOptionPane.showMessageDialog(null, "Файл " + fileName + " не был создан.");
                }
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    private class onClickBtnSettings implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Settings dialog = new Settings(param);
            dialog.pack();
            agt = new Agent();
            if (dialog.showDialog() == 1){
                param = dialog.getParameters();
                //System.out.print(param);
                agt.setParameters(param);
            }
        }
    }

    private class onClickBtnLaunch implements ActionListener{
        public void actionPerformed(ActionEvent e){
            startProcessConveyor();
        }
    }

    private class onClickBtnStat implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (agt == null || !agt.isTaskDone()) {
                JOptionPane.showMessageDialog(null, "Сначала нужно выполнить программу (кнопка Пуск).");
                return;
            }
            Statistics stc = new Statistics(
                                                agt.getVariant(),
                                                agt.getNumOfBlocks(),
                                                agt.getConveyors().get(0).size(),
                                                agt.hasAdditionalStages(),
                                                agt.hasDuplicateStage(),
                                                agt.hasProhibitedReuse(),
                                                agt.hasPrediction(),
                                                agt.hasSameLength()
                                            );
            stc.pack();
        }
    }

    private class onClickBtnSave implements ActionListener{
        public void actionPerformed(ActionEvent e){
            saveProcessResult();
        }
    }

    private class onClickBtnEditor implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Editor editor = new Editor();
            editor.setSize(new Dimension(1000, 400));
            editor.pack();
        }
    }

    private class onClickBtnHelp implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Help help = new Help(isAdministrator);
            help.setSize(new Dimension(850, 600));
            help.pack();
        }
    }

    private class onClickBtnClose implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.gc();
            System.exit(0);
        }
    }

    private void setHeaders(int stageNum, int convNum){
        try {
            int _length = stageNum*convNum + 1;
            //System.out.println("Length: " + _length);
            headers = new Object[_length];
            headers[0] = "Такты";
            if (param.duplicateStage && param.splitFirst){
                headers[1] = "Ст.1 " + "Вычисление адреса команды";
                headers[2] = "Ст.2 " + "Предвыборка блока команд";
                headers[3] = "Ст.3 " + "Выборка команды";
                headers[4] = "Ст.4 " + "Декодирование команды";
                headers[5] = "Ст.5 " + "Выборка операндов";
                headers[6] = "Дуб.Ст.5 " + "Выборка операндов";
                headers[7] = "Ст.6 " + "Выполнение команды";
                headers[8] = "Ст.7 " + "Запись результатов";
            }
            else if (param.duplicateStage){
                headers[1] = "Ст.1 " + "Выборка команды";
                headers[2] = "Ст.2 " + "Декодирование команды";
                headers[3] = "Ст.3 " + "Выборка операндов";
                headers[4] = "Дуб.Ст.3 " + "Выборка операндов";
                headers[5] = "Ст.4 " + "Выполнение команды";
                headers[6] = "Ст.5 " + "Запись результатов";
            }
            else if (param.splitFirst){
                headers[1] = "Ст.1 " + "Вычисление адреса команды";
                headers[2] = "Ст.2 " + "Предвыборка блока команд";
                headers[3] = "Ст.3 " + "Выборка команды";
                headers[4] = "Ст.4 " + "Декодирование команды";
                headers[5] = "Ст.5 " + "Выборка операндов";
                headers[6] = "Ст.6 " + "Выполнение команды";
                headers[7] = "Ст.7 " + "Запись результатов";
            }
            else {
                headers[1] = "Ст.1 " + "Выборка команды";
                headers[2] = "Ст.2 " + "Декодирование команды";
                headers[3] = "Ст.3 " + "Выборка операндов";
                headers[4] = "Ст.4 " + "Обработка данных";
                headers[5] = "Ст.5 " + "Запись результатов";
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
