import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by crystall on 28.01.2018.
 */
public class Login extends JDialog {
    private JPanel rootPanel;
    private JPanel Header;
    private JPanel Body;
    private JButton btn_user;
    private JButton btn_admin;
    private int result;

    public Login(){

        setContentPane(rootPanel);
        setModal(true);
        setSize(new Dimension(125, 100));
        //getRootPane().setDefaultButton(btn_user);
        //getRootPane().setDefaultButton(btn_admin);

        btn_user.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onUser();
            }
        });

        btn_admin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onAdmin();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });


    }

    private void onCancel() {
        result = -1;
        dispose();

    }

    private void onUser() {
        result = 0;
        dispose();
    }

    private void onAdmin() {
        result = 1;
        dispose();
    }

    public int showDialog(){
        setVisible(true);
        return result;
    }

}
