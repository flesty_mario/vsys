import java.util.ArrayList;

/**
 * Created by crystall on 02.11.2017.
 */
public class Conveyor {
    private ArrayList<Block> blocks;
    private int totalNumTakts;

    public int startIndex;
    public int currentIndex;

    private Conveyor() {
        blocks = new ArrayList<>();
        totalNumTakts = 0;
        startIndex = 0;
        currentIndex = 0;
    }

    Conveyor(int _numOfBlocks){
        this();
        for (int _i = 0; _i < _numOfBlocks; _i++){
            blocks.add(new Block());
        }
    }

    public int getTotalNumTakts(){
        return totalNumTakts;
    }

    public ArrayList<Block> getBlocks(){
        return blocks;
    }

    public Block getBlock(int _index){
        return blocks.get(_index % blocks.size());
    }

    public void addEmptyLine(int _startIndex){
        for(int _i = _startIndex; _i < blocks.size(); _i++){
            blocks.get(_i).addEmptyLine();
        }
    }

    public void addEmptyLine(){
        for(Block _block: blocks){
            _block.addEmptyLine();
        }
    }

    public int size(){
        return blocks.get(blocks.size()-1).length();
    }

    public int getLastIndex() { return blocks.get(blocks.size()-1).getLastIndex(); }

    public int getLastIndex(int blockIndex) { return blocks.get(blockIndex).getLastIndex(); }

    public void clearAll(){
        for (Block _stage: blocks){
            _stage.clear();
        }
    }

    public void removeLine(int _index){
        for(Block _block: blocks){
            _block.removeAt(_index);
        }
    }

    public boolean isEmptyLine(int _index, int _takt){
        boolean result = true;
        for (int _i = _index; _i < blocks.size(); _i++){
            result = result && blocks.get(_i).isEmptyLine(_takt);
        }
        return result;
    }
}
