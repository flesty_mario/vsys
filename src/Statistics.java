import javax.swing.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by crystall on 11.11.2017.
 */
public class Statistics extends JFrame{
    private JPanel rootPanel;
    private JPanel header;
    private JPanel main;
    private JPanel current;
    private JPanel other;
    private JLabel lblVariant;
    private JLabel lblAdditionalStages;
    private JLabel lblNumStages;
    private JLabel lblNumTakt;
    private JComboBox fileToCompare;
    private JLabel lbl_varToCmp;
    private JLabel lbl_stepToCmp;
    private JLabel lbl_splitToCmp;
    private JLabel lbl_taktToCmp;
    private JLabel lblPredictions;
    private JLabel lblSameLength;
    private JLabel lblDuplicateStage;
    private JLabel lblReuseProhibited;
    private JLabel lbl_predToCnp;
    private JLabel lbl_lengthToCmp;
    private JLabel lbl_duplToCmp;
    private JLabel lbl_reuseToCmp;

    private String variantToCompare;
    private String AdditionalStages;
    private String numOfBlockToCompare;
    private String numOfTaktsToCompare;
    private String duplicateStage;
    private String ProhibitedReuse;
    private String Prediction;
    private String SameLength;

    Statistics(){
        setContentPane(rootPanel);
        setSize(500, 250);
        getAllSavedProcessing();
        fileToCompare.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loadProcessResults(fileToCompare.getSelectedItem().toString());
                lbl_varToCmp.setText("Вариант: " + variantToCompare);
                lbl_splitToCmp.setText("Увеличение числа ступеней: " + (AdditionalStages.equals("Y") ? "Да" : "Нет"));
                lbl_stepToCmp.setText("Число шагов: " + numOfBlockToCompare);
                lbl_taktToCmp.setText("Число тактов: " + numOfTaktsToCompare);
                lbl_duplToCmp.setText("Дублирование сложной ступени: " + (duplicateStage.equals("Y") ? "Да" : "Нет"));
                lbl_reuseToCmp.setText("Отмена повторного чтения: " + (ProhibitedReuse.equals("Y") ? "Да" : "Нет"));
                lbl_predToCnp.setText("Предсказание переходов: " + (Prediction.equals("Y") ? "Да" : "Нет"));
                lbl_lengthToCmp.setText("Одинаковая длина фаз: " + (SameLength.equals("Y") ? "Да" : "Нет"));
            }
        });
    }

    private void loadProcessResults(String fileName){
        try{
            File file = new File("./results/" + fileName);
            FileReader fread = new FileReader(file);
            BufferedReader bin = new BufferedReader(fread);
            variantToCompare = bin.readLine();
            numOfBlockToCompare = bin.readLine();
            numOfTaktsToCompare = bin.readLine();
            AdditionalStages = bin.readLine();
            duplicateStage = bin.readLine();
            ProhibitedReuse = bin.readLine();
            Prediction = bin.readLine();
            SameLength = bin.readLine();
            fread.close();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    private void getAllSavedProcessing(){
        File dir = new File("./results");
        for (File item: dir.listFiles()){
            fileToCompare.addItem(item.getName());
        }
    }

    Statistics(int variant, int numSteps, int numTakt, String splitStage, String doubleStage, String phbReuse, String pred, String sameTakts){
        this();
        lblVariant.setText("Вариант: " + variant);
        lblAdditionalStages.setText("Увеличение числа ступеней: " + (splitStage.equals("Y") ? "Да" : "Нет"));
        lblNumStages.setText("Число шагов: " + numSteps);
        lblNumTakt.setText("Число тактов: " + numTakt);
        lblDuplicateStage.setText("Дублирование сложной ступени: " + (doubleStage.equals("Y") ? "Да" : "Нет"));
        lblReuseProhibited.setText("Отмена повторного чтения: " + (phbReuse.equals("Y") ? "Да" : "Нет"));
        lblPredictions.setText("Предсказание переходов: " + (pred.equals("Y") ? "Да" : "Нет"));
        lblSameLength.setText("Одинаковая длина фаз: " + (sameTakts.equals("Y") ? "Да" : "Нет"));
        setVisible(true);
    }
}
