/**
 * Created by crystall on 02.11.2017.
 */
import java.lang.String;
import java.util.ArrayList;

public class Command {
    private String name;
    private String comment;
    private int taktCount;
    private ArrayList<Operand> operands;
    private ArrayList<Flag> operandList;
    private ArrayList<Flag> flagList;
    private ArrayList<Flag> registerList;
    private Operand gotoLabel;
    private boolean repeat; //повторное исполнение (цикл)

    Command(){
        taktCount = 0;
        comment = "";
        gotoLabel = null;
        repeat = false;
        operands = new ArrayList<>();
        operandList = new ArrayList<>();
        flagList = new ArrayList<>();
        registerList = new ArrayList<>();
    }

    Command(String _name, int _taktCount){
        this();
        name = _name;
        taktCount = _taktCount;
    }

    public String getName(){
        return name;
    }

    public void setName(String _name) { name = _name; }

    public Operand getGotoLabel(){ return gotoLabel; }

    public void setGotoLabel(Operand _label){
        gotoLabel = (!_label.getName().equals("")) ? _label : null;
    }

    public void setComment(String _comment){ comment = _comment; }

    public String getComment() { return comment; }

    public String toString(){
        return (gotoLabel != null ? gotoLabel + ":": "") + name + (getOperand(0) != null && getOperand(0).isVisible ? " "  + getOperand(0) :"") + (getOperand(1) != null && getOperand(1).isVisible ? "," + getOperand(1) :"");
    }

    public String getFullDescription(){
        return toString() + ((!comment.equals("")) ? "; " + comment : "");
    }

    public int getTaktCount(){
        return taktCount;
    }

    public void resetReadedOperands(){
        for (Flag operand: operandList){
            //System.out.println((isRepeCommand() || isMoveCommand() || name.equals("RET")));
            if ((!isProcedureCommand() && !isRepeCommand() &&  !isLea() && !isMoveCommand() && !name.equals("RET") || operandList.indexOf(operand)!=0) && !isStackControl()) {
                //System.out.println("CMD: " +name+" Operand: " + operand.opr.getName() + " is reset");
                operand.isReaded = false;
            }
        }
        for (Flag operand: flagList){
            operand.isReaded = false;
            //System.out.println("CMD: " +name+" FLAG: " + operand.opr.getName() + " is reset");
        }
        for (Flag operand: registerList){
            operand.isReaded = false;
            //System.out.println("CMD: " +name+" Register: " + operand.opr.getName() + " is reset");
        }
    }

    /*public Operand getOperand(int _index){
        return (_index < operands.size() ? operands.get(_index % operands.size()) : null);
    }*/

    public Flag getOperand(int _index){
        return (_index < operandList.size() ? operandList.get(_index % operandList.size()) : null);
    }

    public Flag getRegister(int _index){
        return (_index < registerList.size() ? registerList.get(_index % registerList.size()) : null);
    }

    public Flag getFlag(int _index){
        return (_index < flagList.size() ? flagList.get(_index % flagList.size()) : null);
    }

    public boolean hasOperand(String register){
        for (Flag reg: registerList){
            if (reg.opr.getName().equals(register)){
                return true;
            }
        }
        for (Flag reg: operandList){
            if (reg.opr.getName().equals(register)){
                return true;
            }
        }
        for (Flag reg: flagList){
            if (reg.opr.getName().equals(register)){
                return true;
            }
        }
        return false;
    }

    /*public ArrayList<Operand> getOperands(){
        return operands;
    }*/

    private void sortOrder(ArrayList<Flag> arr){
        for (int _i = 0; _i < arr.size(); _i++){
            int min = arr.get(_i).index;
            int minIndex = _i;
            for (int _j = _i+1; _j < arr.size(); _j++){
                if (arr.get(_j).index < min){
                    min = arr.get(_j).index;
                    minIndex = _j;
                }
            }
            if (_i != minIndex){
                Flag tmp = arr.get(_i);
                //arr.get(_i)
                arr.add(_i, arr.get(minIndex));
                arr.remove(_i+1);

                arr.add(minIndex, tmp);
                arr.remove(minIndex+1);
            }
        }
    }

    public ArrayList<Flag> getOperands() { return operandList; }

    public ArrayList<Flag> getFlags() { return flagList; }

    public ArrayList<Flag> getRegisters() { return registerList; }

    public ArrayList<Flag> getAllOperands() {
        ArrayList<Flag> operandsFlagsRegisters = new ArrayList<Flag>();
        operandsFlagsRegisters.addAll(operandList);
        operandsFlagsRegisters.addAll(registerList);
        operandsFlagsRegisters.addAll(flagList);
        sortOrder(operandsFlagsRegisters);
        return operandsFlagsRegisters;
    }

    public ArrayList<Flag> getDistinctOperands(){
        boolean sameFound = false;
        ArrayList<Flag> distinctOperandList = new ArrayList<>();
        for (Flag f: operandList){
            sameFound = false;
            for (Flag disf: distinctOperandList){
                if (disf.opr.getName().equals(f.opr.getName())){
                    sameFound = true;
                    break;
                }
            }
            if (!sameFound){
                distinctOperandList.add(f);
            }
        }
        distinctOperandList.addAll(registerList);
        distinctOperandList.addAll(flagList);
        sortOrder(distinctOperandList);
        return distinctOperandList;
    }

    public void addOperand(Operand _op) {
        operandList.add(new Flag(_op, "OPERAND"));
        operandList.get(operandList.size()-1).index = (operandList.size() + registerList.size() + flagList.size());
        operandList.get(operandList.size()-1).isReaded = false;
    }

    public void addOperand(int _index, Operand _op){
        Flag opr = null;
        if (!_op.getName().equals("")){
            opr = new Flag(_op, "OPERAND");
        }
        if (opr == null) return;
        if (_index < operandList.size()){
            operandList.add(_index, opr);
            operandList.remove(_index+1);
        }
        else{
            operandList.add(opr);
        }
        operandList.get(_index).index = _index;
        operandList.get(operandList.size()-1).isReaded = false;
    }

    public void addFlag(Operand _op) {
        flagList.add(new Flag(_op, "FLAG"));
        flagList.get(flagList.size()-1).index = (operandList.size() + registerList.size() + flagList.size());
        flagList.get(flagList.size()-1).isReaded = false;
    }

    public void addRegister(Operand _op) {
        registerList.add(new Flag(_op, "REGISTER"));
        registerList.get(registerList.size()-1).index = (operandList.size() + registerList.size() + flagList.size());
        registerList.get(registerList.size()-1).isReaded = false;
    }

    public boolean noResult() { return name.equals("TEST") ||
                                       name.equals("CMP") ||
                                       name.contains("PUSH") ||
                                       name.equals("CLD") ||
                                       name.contains("STD")  ||
                                       name.contains("CMPSB");
    }

    public boolean isConditionalCommand(){
        return name.charAt(0) == 'J' && !name.equals("JMP");
    }

    public boolean isProcedureCommand(){ return name.equals("PROC"); }

    public boolean isCallFunctionCommand(){ return name.equals("CALL"); }

    public boolean isReturnCommand(){ return name.equals("RET"); }

    public boolean isCLCCommand() { return name.equals("CLC");}

    public boolean isEmptyCommand(){ return name.equals("NOP") || name.equals("PROC"); }

    //ZF, метка, ESI
    public boolean isJumpCommand(){
        return name.equals("JZ");
    }

    public boolean isJCXZCommand(){
        return name.equals("JCXZ");
    }

    public boolean isJMPCommand(){ return name.equals("JMP");}

    public boolean isJNECommand(){ return name.equals("JNE");}

    public boolean isJNCommand() {
        return name.equals("JNE") || name.equals("JNA") || name.equals("JNC") || name.equals("JNZ");
    }

    //SF, AF, операнды
    public boolean isAryphSimple(){
        return name.equals("ADD") || name.equals("SUB");
    }

    //SF, AF, CF, операнды
    public boolean isAryphCarried() {
        return name.equals("ADC") || name.equals("SBB");
    }

    //CX, метка
    public boolean isLoopCommand() { return name.equals("LOOP"); }

    //ZF, метка, ESI
    public boolean isJumpZero() { return name.equals("JZ"); }

    public boolean isCompareCommand() { return name.equals("CMP"); }

    //EDX, EAX, операнды
    public boolean isDivision() { return name.equals("DIV"); }

    public boolean isLea() { return name.equals("LEA"); }

    //EAX, операнды
    public boolean isMultiply() { return name.equals("MUL"); }

    //ESP, операнды
    public boolean isStackControl() { return name.equals("PUSH") || name.equals("POP"); }

    //ESP, PF
    public boolean isStackControlFlags() { return name.equals("PUSHFD") || name.equals("POPFD"); }

    //DF
    public boolean isControlDF() { return name.equals("STD") || name.equals("CLD"); }

    public boolean isRepeCommand() { return name.equals("REPE"); }

    public boolean isMoveCommand() { return name.contains("MOV"); }

    public boolean isXORCommand() { return name.equals("XOR"); }

    public boolean isORCommand() { return name.equals("OR"); }

    public boolean isAndCommand() { return name.equals("AND"); }

    public boolean isShiftCommand() { return name.equals("SHR") || name.equals("SHL"); }

    public boolean isIncDecCommand() { return name.equals("INC") || name.equals("DEC"); }

    public boolean isRepeat() { return repeat; }

    public void setRepeat() { repeat = !repeat; }
}
