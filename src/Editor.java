import javafx.scene.control.Tab;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Vector;

import static com.sun.glass.ui.Cursor.setVisible;

/**
 * Created by crystall on 27.01.2018.
 */
public class Editor extends JFrame {

    private JButton addRow;
    private JPanel MainPanel;
    private JPanel TopPanel;
    private JTable instructions;
    private DefaultTableModel tableModel;
    private JPanel rootPanel;
    private JButton deleteRow;
    private JButton save;
    private JComboBox variantNum;
    private JLabel lbl_variant;
    private JButton deleteVariant;
    private int maxVariant;
    private Object[] headers = {"МЕТКА", "КОМАНДА", "ОПЕРАНД1", "ОПЕРАНД2", "КОММЕНТАРИЙ"};
    private int[] headerSize = {100, 100, 150, 150, 500};

    ArrayList<Command> commandList;

    public Editor() {
        ActionListener btnAddRowClickListener = new onClickBtnAddRow();
        addRow.addActionListener(btnAddRowClickListener);

        ActionListener btnDeleteRowClickListener = new onClickBtnDeleteRow();
        deleteRow.addActionListener(btnDeleteRowClickListener);

        ActionListener btnSaveClickListener = new onClickBtnSave();
        save.addActionListener(btnSaveClickListener);

        ActionListener btnDeleteVariantClickListener = new onClickBtnDeleteVariant();
        deleteVariant.addActionListener(btnDeleteVariantClickListener);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        JComboBox comboBoxCommandList = new JComboBox();
        comboBoxCommandList.addItem("MOV");
        comboBoxCommandList.addItem("MOVZX");
        comboBoxCommandList.addItem("AND");
        comboBoxCommandList.addItem("SUB");
        comboBoxCommandList.addItem("OR");
        comboBoxCommandList.addItem("XOR");
        comboBoxCommandList.addItem("NOT");
        comboBoxCommandList.addItem("NEG");
        comboBoxCommandList.addItem("JMP");
        comboBoxCommandList.addItem("JZ");
        comboBoxCommandList.addItem("JNE");
        comboBoxCommandList.addItem("JNA");
        comboBoxCommandList.addItem("JNC");
        comboBoxCommandList.addItem("JNZ");
        comboBoxCommandList.addItem("JCXZ");
        comboBoxCommandList.addItem("SHL");
        comboBoxCommandList.addItem("SHR");
        comboBoxCommandList.addItem("TEST");
        comboBoxCommandList.addItem("ADD");
        comboBoxCommandList.addItem("ADC");
        comboBoxCommandList.addItem("SUB");
        comboBoxCommandList.addItem("SBB");
        comboBoxCommandList.addItem("MUL");
        comboBoxCommandList.addItem("DIV");
        comboBoxCommandList.addItem("MOVSX");
        comboBoxCommandList.addItem("CMP");
        comboBoxCommandList.addItem("INC");
        comboBoxCommandList.addItem("DEC");
        comboBoxCommandList.addItem("PUSH");
        comboBoxCommandList.addItem("POP");
        comboBoxCommandList.addItem("PUSHFD");
        comboBoxCommandList.addItem("POPFD");
        comboBoxCommandList.addItem("LOOP");
        comboBoxCommandList.addItem("CLD");
        comboBoxCommandList.addItem("CLC");
        comboBoxCommandList.addItem("STD");
        comboBoxCommandList.addItem("CMPSB");
        comboBoxCommandList.addItem("CALL");
        comboBoxCommandList.addItem("RET");
        comboBoxCommandList.addItem("NOP");
        comboBoxCommandList.addItem("LEA");
        comboBoxCommandList.addItem("REPE");
        comboBoxCommandList.addItem("PROC");

        commandList = new ArrayList<>();

        getAllVariants();

        setContentPane(rootPanel);
        setVisible(true);

        MainPanel.removeAll();
        MainPanel.setLayout(new GridLayout());
        JFrame jfrm = new JFrame("Editor");
        jfrm.getContentPane().setLayout(new FlowLayout());
        jfrm.setSize(1000, 400);

        JScrollPane jscrlp = new JScrollPane(instructions);

        tableModel = new DefaultTableModel();
        for (int i = 0; i < headers.length; i++){
            tableModel.addColumn(headers[i]);
        }
        instructions.setModel(tableModel);
        instructions.getColumn(headers[0]).setWidth(headerSize[0]);
        instructions.getColumn(headers[0]).setMaxWidth(headerSize[0]);
        instructions.getColumn(headers[0]).setMinWidth(headerSize[0]);
        instructions.getColumn(headers[1]).setWidth(headerSize[1]);
        instructions.getColumn(headers[1]).setMaxWidth(headerSize[1]);
        instructions.getColumn(headers[1]).setMinWidth(headerSize[1]);
        instructions.getColumn(headers[1]).setCellEditor(new DefaultCellEditor(comboBoxCommandList));
        instructions.getColumn(headers[2]).setWidth(headerSize[2]);
        instructions.getColumn(headers[2]).setMaxWidth(headerSize[2]);
        instructions.getColumn(headers[2]).setMinWidth(headerSize[2]);
        instructions.getColumn(headers[3]).setWidth(headerSize[3]);
        instructions.getColumn(headers[3]).setMaxWidth(headerSize[3]);
        instructions.getColumn(headers[3]).setMinWidth(headerSize[3]);
        instructions.getColumn(headers[4]).setWidth(headerSize[4]);
        instructions.getColumn(headers[4]).setMinWidth(headerSize[4]);

        MainPanel.add(jscrlp);
        MainPanel.updateUI();

        variantNum.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (variantNum.getSelectedIndex() != -1) {
                    loadVariant(variantNum.getSelectedItem().toString());
                }
            }
        });

    }

    private void onCancel() {
        dispose();
    }

    private void getAllVariants(){
        variantNum.removeAllItems();
        variantNum.addItem("Новый");
        File dir = new File("./variants");
        maxVariant = 0;
        for (File item: dir.listFiles()){
            variantNum.addItem(item.getName().replace(".txt", ""));
            if (Integer.parseInt(item.getName().replace(".txt", "")) > maxVariant){
                maxVariant = Integer.parseInt(item.getName().replace(".txt", ""));
            }
        }
    }

    private void loadVariant(String fileName){
        tableModel.getDataVector().removeAllElements();
        tableModel.rowsRemoved(new TableModelEvent(tableModel));

        commandList = new ArrayList<>();

        if (variantNum.getSelectedIndex() != 0) {
            try {
                File file = new File("./variants/" + fileName + ".txt");
                FileReader fread = new FileReader(file);
                BufferedReader bin = new BufferedReader(fread);
                String line;
                while ((line = bin.readLine()) != null) {
                    int commentIndex = line.indexOf(';');
                    String[] attributes = (commentIndex != -1) ? line.substring(0, commentIndex).split(" ") : line.split(" ");
                    Command cmd;
                    if (attributes[0].contains(":")){
                        cmd = new Command(attributes[0].split(":")[1].toUpperCase().trim(), 0);
                        cmd.setGotoLabel(new Operand(attributes[0].split(":")[0].toUpperCase().trim()));
                    }
                    else{
                        cmd = new Command(attributes[0].toUpperCase().trim(), 0);
                    }
                    if (commentIndex != -1) {
                        cmd.setComment(line.substring(commentIndex, line.length()).replace(";", " ").trim());
                    }
                    if (attributes.length > 1) {
                        String[] operands = attributes[1].split(",");
                        for (String _op : operands) {
                            cmd.addOperand(new Operand(_op));
                        }
                    }
                    commandList.add(cmd);
                    Vector newRow = new Vector();
                    if (cmd.getGotoLabel() != null){
                        newRow.add(cmd.getGotoLabel().getName());
                    }
                    else{
                        newRow.add("");
                    }
                    newRow.add(cmd.getName());
                    if (cmd.getOperand(0) != null) {
                        newRow.add(cmd.getOperand(0));
                    }
                    else {
                        newRow.add("");
                    }
                    if (cmd.getOperand(1) != null) {
                        newRow.add(cmd.getOperand(1));
                    }
                    else{
                        newRow.add("");
                    }
                    newRow.add(cmd.getComment());
                    tableModel.addRow(newRow);
                    tableModel.newRowsAdded(new TableModelEvent(tableModel));
                }
                fread.close();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class onClickBtnAddRow implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (instructions.getSelectedRow() == -1 || instructions.getSelectedRow() == instructions.getRowCount()-1) {
                tableModel.addRow(new Vector());
                commandList.add(new Command());
            }
            else {
                tableModel.insertRow(instructions.getSelectedRow()+1, new Vector());
                commandList.add(instructions.getSelectedRow()+1,new Command());
            }
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
            //System.out.println(commandList.size());
        }
    }

    private class onClickBtnDeleteRow implements ActionListener{
        public void actionPerformed(ActionEvent e){
            int row = instructions.getSelectedRow();
            if (row != -1) {
                commandList.remove(row);
                tableModel.removeRow(row);
            }
        }
    }

    private class onClickBtnSave implements ActionListener{
        public void actionPerformed(ActionEvent e){
            for (int i = 0; i < tableModel.getRowCount(); i++){
                for (int j = 0; j < tableModel.getColumnCount(); j++){
                    String value = (tableModel.getValueAt(i, j) != null) ? tableModel.getValueAt(i, j).toString() : "";
                    //System.out.println("SETTING VALUE IN ROW: " + i + " COLUMN: " + j + "=== " + value);
                    if (j==0) commandList.get(i).setGotoLabel(new Operand(value.replaceAll(" ","").trim()));
                    if (j==1) commandList.get(i).setName(value.trim());
                    if (j==2) commandList.get(i).addOperand(0, new Operand(value.replaceAll(" ","").trim()));
                    if (j==3) commandList.get(i).addOperand(commandList.get(i).getOperands().size(), new Operand(value.replaceAll(" ","").trim()));
                    if (j==4) commandList.get(i).setComment(value);
                }
                //System.out.println("COMMAND ADDED: " + commandList.get(i));
            }
            try {
                String fileName;
                if (variantNum.getSelectedIndex() == 0) {
                    fileName = "./variants/" + (maxVariant+1) + ".txt";
                }
                else{
                    fileName = "./variants/" + variantNum.getSelectedItem().toString() + ".txt";
                }
                File file = new File(fileName);
                file.delete();
                if (!file.createNewFile()){
                    throw new Exception("Файл не был создан");
                }
                FileWriter fout = new FileWriter(file, true);
                for (Command _cmd: commandList){
                    fout.write("" + _cmd.getFullDescription());
                    fout.write("\r\n");
                }
                fout.close();
                if (variantNum.getSelectedIndex() == 0) {
                    getAllVariants();
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private class onClickBtnDeleteVariant implements ActionListener{
        public void actionPerformed(ActionEvent e){
            try {
                if (variantNum.getSelectedIndex() > 0) {
                    String fileName = "./variants/" + variantNum.getSelectedItem().toString() + ".txt";
                    File file = new File(fileName);
                    if (!file.delete()) {
                        throw new Exception("Файл не был удален");
                    }
                    getAllVariants();
                    variantNum.setSelectedIndex(0);
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

}
