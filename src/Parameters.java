/**
 * Created by crystall on 06.11.2017.
 */
public class Parameters {
    public boolean positiveFlags;
    public boolean sameTakt;
    public boolean splitFirst;
    public boolean discardCash;
    public boolean duplicateStage;
    public boolean addPredict;
    public boolean reuseProhibited;
    public int convNum;
    public int varNum;
    public String ins;

    Parameters(){}

    public String toString(){
        return "Settings: \nCount of Conveyor: " + convNum + "\nNumber of Variant: " + varNum + "\nPrediction Added: " + addPredict
                + "\nPositive Flags: " + positiveFlags + "\nSame Count of Takts: " + sameTakt + "\nDiscard Cash Memory: " + discardCash
                + "\nSplit First stage: " + splitFirst + "\nReuse prohibited: " + reuseProhibited + "\nDuplicate stage 3: " + duplicateStage + "\n\nInstructions: \n"+ ins;
    }
}
