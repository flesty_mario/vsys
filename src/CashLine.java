/**
 * Created by crystall on 05.11.2017.
 */
public class CashLine {
    private int tag;
    private Operand operand;

    CashLine(){
        tag = -1;
        operand = null;
    }

    CashLine(int _tag, Operand _operand){
        tag = _tag;
        operand = _operand;
    }

    public int getTag(){ return tag; }

    public Operand getOperand() { return operand; }
}
