/**
 * Created by crystall on 04.11.2017.
 */
//тип операнда
public enum AddressType {
    CONSTANT,     //константа
    INTERNAL_MEM, //КЭШ-память
    EXTERNAL_MEM, //внешняя память (ОП)
    ADDRESS_MEM,  //Адрес перехода
    DEFAULT
}
