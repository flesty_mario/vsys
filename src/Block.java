import java.util.ArrayList;

/**
 * Created by crystall on 02.11.2017.
 */

public class Block {
    private ArrayList<Command> task;
    private ArrayList<String> taskDescription;
    private int totalWorkTime;
    private boolean status;

    Block(){
        task = new ArrayList<>();
        taskDescription = new ArrayList<>();
        totalWorkTime = 0;
        status = false;
    }

    public void setStatus(){
        status = !status;
    }

    public boolean isWorking(){
        return status == true;
    }

    public int getTotalWorkTime(){
        return totalWorkTime;
    }

    public void addCommand(Command _task){
        task.add(_task);
    }

    public void addEmptyLine(){
        task.add(null);
        taskDescription.add("");
    }

    public void setCommand(Command _task){
        task.set(task.size()-1, _task);
    }

    public void setCommand(Command _task, String _desc, int _index){
        task.set(_index, _task);
        taskDescription.set(_index, _desc);
    }

    public Command getCommand(){
        return task.get(task.size() - 1);
    }

    public String getCommandDescription(int _index) { return (_index >= 0) ? taskDescription.get(_index) : null; }

    public Command getCommand(int _index) { return (_index >= 0) ? task.get(_index) : null; }

    public void removeAt(int _index){
        task.remove(_index);
    }

    public int length(){
        return task.size();
    }

    public void clear(){
        task.clear();
    }

    public int getLastIndex() {
        for (int _i = task.size()-1; _i >= 0; _i--){
            if (task.get(_i) != null){
                return _i;
            }
        }
        return -1;
    }

    public boolean isEmptyLine(int _index){
        return task.get(_index) == null;
    }
}
