import java.io.File;
import java.io.FileWriter;

/**
 * Created by crystall on 02.11.2017.
 */
public class Main {

    public static void main(String[] args){

        //Первый запуск
        try {
            File dir = new File("./variants");
            if (!dir.exists()) {
                if (dir.mkdir()){
                    for (int i = 0; i < Variants.variants.length; i++){
                        File variant = new File("./variants/" + (i+1) + ".txt");
                        if (variant.createNewFile()){
                            FileWriter fout = new FileWriter(variant, true);
                            fout.write(Variants.variants[i]);
                            fout.close();
                        }
                        else{
                            System.out.println("Не могу создать файл " + (i+1) + ".txt");
                        }
                    }
                }
                else{
                    throw new Exception("Не могу создать каталог varians");
                }
            }
            dir = new File("./results");
            if (!dir.exists()){
                if (!dir.mkdir()){
                    throw new Exception("Не могу создать каталог results");
                }
            }

            //вход в программу
            Login login = new Login();
            login.pack();
            int res = login.showDialog();
            if (res == 1) {
                new ProcessConveyorMain(true);
            }
            if (res == 0) {
                new ProcessConveyorMain(false);
            }
            if (res == -1){
                System.exit(0);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
