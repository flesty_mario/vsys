/**
 * Created by crystall on 06.11.2017.
 */
public class Flag {
    public Operand opr;
    public String accessMode;
    public boolean isReaded;
    public boolean isVisible;
    public int index;
    public String type;

    Flag(){
        accessMode = "r";
    }

    Flag(Operand _opr, String _type){
        this();
        opr = _opr;
        isReaded = false;
        isVisible = true;
        type = _type;
    }

    public boolean isWriteMode(){
        return accessMode.equals("w");
    }

    public String toString() {
        return opr.getName();
    }
}
