import com.sun.prism.shader.Solid_TextureYV12_AlphaTest_Loader;

import javax.swing.table.DefaultTableModel;
import java.util.*;

/**
 * Created by crystall on 02.11.2017.
 */
public class Agent {
    private Map<String, Integer> operationTaktMap; //справочник команд и времени их выполнения

    private ArrayList<Conveyor> conveyors;         //конвейеры
    private ArrayList<Operand> pullFlags;          //пул флагов
    private ArrayList<Operand> pullOperands;       //пул операндов (ОП память)
    private ArrayList<Operand> pullRegister;       //пул регистров
    //зарезервированные регистры:
    //EAX - целое от деления
    //EDX - остаток от деления
    //DX - деление
    //ESP
    //EFLAGS - флаговый регистр
    //CX - количество итераций цикла
    //DF - флаг направления просмотра строк
    //EIP - адрес возврата
    //CF - флаг переноса
    //AL, AX - регистр-аккумулятор (умножение)
    //ZF - флаг нулевого результата
    //<DATA> - константа, непосредственные данные
    private ArrayList<CashLine> cashMemory;        //кэш память
    private Map<String, ArrayList<Command>> pkM;   //пул команд

    private Map<String, ArrayList<Command>> procedureList; //процедуры
    //настройки
    private boolean isSameTaktLength;              //стадии с одинаковой длиной тактов
    private boolean isAdditionalStagesAdded;       //добавление дополнительных ступеней (разбитие стадий)
    private boolean isDiscardCashMemory;           //отмена кэширования
    private boolean isPredictionAdded;             //добавление предсказаний переходов
    private boolean isReuseProhibited;             //запрет повторного использования
    private boolean isDuplicateStage;              //дублирование 3-й ступени
    private int numOfConveyors;                    //число конвейеров
    private int numOfBlocks;                       //число шагов
    private int variant;                           //вариант

    private String instructions;
    private ListIterator<Command> iter;
    private ListIterator<Command> callProcedureIter;
    private ArrayList<String> keys;
    private int keyIndex;
    private int numberOfTakts;
    private int numberOfIterations;
    private int currentIteration;
    private int xPath;  //если флаг поменялся больше 2 раз
    private boolean missingCash; //кэш промах (1 в программу)

    private boolean paramSet; //параметры установлены

    private int stageEncoding; //перекодирование
    private int stagePrepareSelectionCommand; //предвыборка
    private int stageSelectionCommand; //выборка команды
    private int stageDecodingCommand; // декодирование команды
    private int stageSelectionOperands; //выборка операндов
    private int stageSelectionOperandsAdditional; //выборка операндов (дублированная)
    private int stageExecution; //выполнение команды
    private int stageWriting; //запись результатов

    Agent(){
        missingCash = false;
        stageSelectionCommand = 0;
        stageDecodingCommand = 1;
        stageSelectionOperands = 2;
        stageExecution = 3;
        stageWriting = 4;
        currentIteration = 0;
        numberOfIterations = 3;
        operationTaktMap = new HashMap<String, Integer>();
        conveyors = new ArrayList<>();
        keys = new ArrayList<>();
        procedureList = new HashMap<String, ArrayList<Command>>();
        paramSet = false;
        pullOperands = new ArrayList<>();
        pullRegister = new ArrayList<>();
        pkM = new HashMap<String, ArrayList<Command>>();
        pullFlags = new ArrayList<>();
        cashMemory = new ArrayList<>();
        isAdditionalStagesAdded = false;
        isDiscardCashMemory = false;
        isPredictionAdded = false;
        isSameTaktLength = false;
        isReuseProhibited = false;
        isDuplicateStage = false;
        numOfConveyors = 1;
        variant = -1;
        operationTaktMap.put("MOV", 0);
        operationTaktMap.put("MOVZX", 2);
        operationTaktMap.put("AND", 1);
        operationTaktMap.put("SUB", 1);
        operationTaktMap.put("OR", 1);
        operationTaktMap.put("XOR", 1);
        operationTaktMap.put("NOT", 1);
        operationTaktMap.put("NEG", 1);
        operationTaktMap.put("JMP", 2);
        operationTaktMap.put("JZ", 2);
        operationTaktMap.put("JNE", 2);
        operationTaktMap.put("JNA", 2);
        operationTaktMap.put("JNC", 2);
        operationTaktMap.put("JNZ", 1);
        operationTaktMap.put("JCXZ", 2);
        operationTaktMap.put("SHL", 1);
        operationTaktMap.put("SHR", 1);
        operationTaktMap.put("TEST", 1);
        operationTaktMap.put("ADD", 1);
        operationTaktMap.put("ADC", 1);
        operationTaktMap.put("SUB", 1);
        operationTaktMap.put("SBB", 1);
        operationTaktMap.put("MUL", 3);
        operationTaktMap.put("DIV", 3);
        operationTaktMap.put("MOVSX", 2);
        operationTaktMap.put("MOVZX", 2);
        operationTaktMap.put("CMP", 1);
        operationTaktMap.put("INC", 1);
        operationTaktMap.put("DEC", 1);
        operationTaktMap.put("PUSH", 1);
        operationTaktMap.put("POP", 1);
        operationTaktMap.put("PUSHFD", 1);
        operationTaktMap.put("POPFD", 1);
        operationTaktMap.put("LOOP", 2);
        operationTaktMap.put("CLD", 1);
        operationTaktMap.put("CLC", 1);
        operationTaktMap.put("STD", 1);
        operationTaktMap.put("CMPSB", 2);
        operationTaktMap.put("CALL", 2);
        operationTaktMap.put("RET", 3);
        operationTaktMap.put("NOP", 0);
        operationTaktMap.put("LEA", 1);
        operationTaktMap.put("REPE", 2);
        operationTaktMap.put("PROC", 0);
    }

    Agent(Parameters param){
        this();
        isPredictionAdded = param.addPredict;
        isAdditionalStagesAdded = param.splitFirst;
        isDiscardCashMemory = param.discardCash;
        isSameTaktLength = param.sameTakt;
        numOfConveyors = param.convNum;
        variant = param.varNum;
    }

    public ArrayList<Operand> getPullOperands(){
        return pullOperands;
    }

    public ArrayList<Conveyor> getConveyors(){
        return conveyors;
    }

    public ArrayList<CashLine> getCashMemory() { return cashMemory; }

    public void addOperand(Operand _op){
        pullOperands.add(_op);
    }

    public void addConveyor(Conveyor _cnv){
        conveyors.add(_cnv);
    }

    public void addCashLine(CashLine _cashLine) { cashMemory.add(_cashLine); }

    public int getNumOfConveyors(){ return numOfConveyors; }

    public int getNumOfBlocks() { return numOfBlocks; }

    public int getVariant() { return variant; }

    public DefaultTableModel fetchResults(Object[] headers){
        DefaultTableModel tableModel = new DefaultTableModel();
        for (Object header: headers){
            tableModel.addColumn(header);
        }
        for (int _i = 0; _i < conveyors.get(0).size(); _i++){
            Vector row = new Vector();
            row.add("Такт " + (_i+1));
            for (int _j = 0; _j < numOfBlocks; _j++){
                row.add((conveyors.get(0).getBlock(_j).getCommand(_i) == null) ? "" : conveyors.get(0).getBlock(_j).getCommand(_i).toString() + conveyors.get(0).getBlock(_j).getCommandDescription(_i));
            }
            tableModel.addRow(row);
        }
        return tableModel;
    }

    private void clearAllConveyors(){
        System.out.println("start clear");
        for (Conveyor _conv: conveyors){
            _conv.clearAll();
        }
    }

    public void startProcessCommand(){

        clearAllConveyors();
        xPath = 0;
        keyIndex = 0;
        String _key = keys.get(keyIndex);
        iter = pkM.get(_key).listIterator();
        initProcessStages();
        getFlag("ZF").value = (variant == 4 || variant == 8) ? 0 : 1;
        boolean hasElements = true;
        Command _cmd = null;
        while (hasElements){
            for (Conveyor _conv: conveyors){
                if (iter.hasNext()) {
                    _cmd = iter.next();
                    if (!_cmd.isProcedureCommand()) {
                        if (isAdditionalStagesAdded) {
                            processStagePrepareSelection(_conv, _cmd);
                        } else {
                            processStageOne(_conv, _cmd);
                        }
                    }
                }
                if (!iter.hasNext()) {
                    if (keyIndex < keys.size()-1){
                        keyIndex++;
                        //System.out.println("key index: " + keyIndex);
                        _key = keys.get(keyIndex);
                        iter = pkM.get(_key).listIterator();
                    }
                    else{
                        hasElements = false;
                    }
                }
            }
        }
        removeExtraTakts();
    }

    //убирает лишние такты
    private void removeExtraTakts(){
        int maxTaktNum = 0;
        for (int _i = 0; _i < conveyors.size(); _i++){
            if (conveyors.get(_i).currentIndex > maxTaktNum){
                maxTaktNum = conveyors.get(_i).currentIndex;
            }
        }
        for (int _j = 0; _j < conveyors.size(); _j++) {
            for (int _i = conveyors.get(_j).size()-1; _i >= maxTaktNum; _i--) {
                conveyors.get(_j).removeLine(_i);
            }
        }
    }

    //задает начальные настройки и заполняет диаграмму пустыми тактами
    private void initProcessStages(){
        for (Conveyor _conv: conveyors) {
            _conv.currentIndex = 0;
            _conv.startIndex = 0;
            for (int i = 0; i < 500; i++) {
                _conv.addEmptyLine();
            }
        }
    }

    //выборка команды
    private void processStageOne(Conveyor _conv, Command _cmd){
        //System.out.println("CMD: " + _cmd + " STAGE 1  CURRENT INDEX: " + _conv.currentIndex);
        if (!isAdditionalStagesAdded) {
            _conv.currentIndex = _conv.startIndex;
        }
        numberOfTakts = (isAdditionalStagesAdded && !isSameTaktLength) ? 1 : 3;
        boolean added = false;
        for (int _i = 0; _i < numberOfTakts; _i++) {
            _conv.getBlock(stageSelectionCommand).setCommand(_cmd, "", _conv.currentIndex++);
        }
        while (!_conv.getBlock(stageDecodingCommand).isEmptyLine(_conv.currentIndex)) {
            _conv.getBlock(stageSelectionCommand).setCommand(_cmd, "", _conv.currentIndex++);
            added = true;
        }
        if (!isAdditionalStagesAdded) {
            _conv.startIndex = _conv.currentIndex;
        }
        processStageTwo(_conv, _cmd);
    }

    //считывание команды
    private void processStageTwo(Conveyor _conv, Command _cmd){
        //System.out.println("CMD: " + _cmd + " STAGE 2");
        boolean added = false;
        numberOfTakts = isSameTaktLength ? 3 : 1;
        //если есть дублированная 3-я ступень, то проверяем обе ступени
        if (isDuplicateStage){
            while ((!_conv.getBlock(stageSelectionOperands).isEmptyLine(_conv.currentIndex) || _conv.currentIndex <= _conv.getLastIndex(stageSelectionOperands))
                    &&
                    (!_conv.getBlock(stageSelectionOperandsAdditional).isEmptyLine(_conv.currentIndex) || _conv.currentIndex <= _conv.getLastIndex(stageSelectionOperandsAdditional))
                    ) {
                //System.out.println("FOR INDEX " + _cmd.toString() + " == " + _conv.getLastIndex(stageSelectionOperands) + _conv.getLastIndex(stageSelectionOperandsAdditional));
                _conv.getBlock(stageDecodingCommand).setCommand(_cmd, "", _conv.currentIndex++);
                added = true;
            }
            if (!added) { //начало
                for (int _i = 0; _i < numberOfTakts; _i++) {
                    _conv.getBlock(stageDecodingCommand).setCommand(_cmd, "", _conv.currentIndex++);
                }
            }
            //смотрим, какая ступень освободилась
            if (_conv.getBlock(stageSelectionOperands).isEmptyLine(_conv.currentIndex)) {
                //System.out.println("STAGE 3. CMD: " + _cmd);
                processStageThree(_conv, _cmd);
            }
            else if (_conv.getBlock(stageSelectionOperandsAdditional).isEmptyLine(_conv.currentIndex)) {
                //System.out.println("STAGE 3 ADD. CMD: " + _cmd);
                processStageThreeAdditional(_conv, _cmd);
            }
        }
        //если нет дублированной ступени, проверяем только одну
        else {
            while (!_conv.getBlock(stageSelectionOperands).isEmptyLine(_conv.currentIndex) ||
                    _conv.currentIndex <= _conv.getLastIndex(stageSelectionOperands)
                    ) {
                _conv.getBlock(stageDecodingCommand).setCommand(_cmd, "", _conv.currentIndex++);
                added = true;
            }
            if (!added) { //начало
                for (int _i = 0; _i < numberOfTakts; _i++) {
                    _conv.getBlock(stageDecodingCommand).setCommand(_cmd, "", _conv.currentIndex++);
                }
            }
            processStageThree(_conv, _cmd);
        }
    }

    //выборка операндов
    private void processStageThree(Conveyor _conv, Command _cmd){
        //System.out.println("CMD: " + _cmd + " STAGE 3");
        if (isSameTaktLength){
            for (int _i = 0; _i < 3; _i++){
                _conv.getBlock(stageSelectionOperands).setCommand(_cmd, "", _conv.currentIndex++);
            }
            processStageFour(_conv, _cmd);
        }
        else {
            _cmd.resetReadedOperands();
            ArrayList<Flag> _op;
            Flag currentOperand = null;
            //выборка операндов, в зависимости от команды
            if (isReuseProhibited) {
                _op = _cmd.getDistinctOperands();
            } else {
                //любая другая команда - ее собственные операнды
                _op = _cmd.getAllOperands();
            }
            boolean registerFlagReaded = false;
            int _i = 0;
            while (countReadedOperands(_op) < _op.size()) {
                currentOperand = _op.get(_i % _op.size());
                //System.out.println("STEP 3: CMD " + _cmd.getName() + " OPERAND " + currentOperand.opr.getName() + " AVAILABILITY INDEX: " + currentOperand.opr.availabilityIndex + " CURRENT INDEX: " + _conv.currentIndex);
                if ((_conv.currentIndex <= currentOperand.opr.availabilityIndex && countUnreadedOperands(_op) > countLockedOperands(_conv, _op)) || currentOperand.isReaded) {
                    _i++;
                    continue;
                }
                while (_conv.currentIndex < currentOperand.opr.availabilityIndex) {
                    //System.out.println("System waiting for release operand: " + currentOperand.opr.getName());
                    _conv.getBlock(stageSelectionOperands).setCommand(_cmd, " - чт. " + (!currentOperand.type.equals("FLAG") ? currentOperand.opr.getName() : "FLAGS"), _conv.currentIndex++);
                }
                //System.out.println("КЭШ ПОПАДАНИЕ ДЛЯ: " + _op.get(_i).opr.toString() + " == " + indexInCashMemory(_op.get(_i).opr) + " " + _op.get(_i).opr.isInternalMemory() + " " + _op.get(_i).accessMode.equals("w"));
                //константа
                if (currentOperand.opr.isConstant() || currentOperand.opr.isOffset() || (_cmd.isLea() && currentOperand.opr.isInternalMemory())) {
                    numberOfTakts = isSameTaktLength ? 3 : 1;
                }
                //регистр или флаг
                else if (indexInRegister(currentOperand.opr.getName()) != -1 || indexInFlags(currentOperand.opr) != -1) {
                    numberOfTakts = isSameTaktLength ? 3 : 1;
                }
                //кэш-промах
                else if (!missingCash && currentOperand.opr.isInternalMemory() && indexInCashMemory(currentOperand.opr) == -1) {
                    numberOfTakts = isSameTaktLength ? 3 : 10;
                    missingCash = !missingCash;
                }
                //кэш-попадание
                else {
                    numberOfTakts = 3;
                    cashMemory.add(new CashLine(_i, currentOperand.opr));
                }
                for (int _x = 0; _x < numberOfTakts; _x++) {
                    if (currentOperand.type.equals("FLAG") && registerFlagReaded) {
                        continue;
                    } else {
                        _conv.getBlock(stageSelectionOperands).setCommand(_cmd, " - чт. " + (!currentOperand.type.equals("FLAG") ? currentOperand.opr.getName() : "FLAGS"), _conv.currentIndex++);
                    }
                }
                currentOperand.isReaded = true;
                if (currentOperand.type.equals("FLAG")) {
                    registerFlagReaded = true;
                }
                _i++;
            }
            //ждать, пока освободится сл. ступ.
            while (!_conv.getBlock(stageExecution).isEmptyLine(_conv.currentIndex) || _conv.currentIndex < _conv.getLastIndex(stageExecution)) {
                if (currentOperand != null) {
                    _conv.getBlock(stageSelectionOperands).setCommand(_cmd, " - чт. " + (!currentOperand.type.equals("FLAG") ? currentOperand.opr.getName() : "FLAGS"), _conv.currentIndex++);
                    currentOperand.isReaded = true;
                    _i++;
                } else {
                    _conv.currentIndex++;
                }
            }
            _op = null;
            processStageFour(_conv, _cmd);
        }
    }

    private void processStageThreeAdditional(Conveyor _conv, Command _cmd){
        if (isSameTaktLength){
            for (int _i = 0; _i < 3; _i++){
                _conv.getBlock(stageSelectionOperandsAdditional).setCommand(_cmd, "", _conv.currentIndex++);
            }
            processStageFour(_conv, _cmd);
        }
        _cmd.resetReadedOperands();
        ArrayList<Flag> _op;
        Flag currentOperand = null;

        //запрет повторного выбора
        if (isReuseProhibited){
            _op = _cmd.getDistinctOperands();
        }
        else{
            _op = _cmd.getAllOperands();
        }

        boolean registerFlagReaded = false;
        int _i =0;
        while (countReadedOperands(_op) < _op.size()){
            currentOperand = _op.get(_i%_op.size());
            if ((_conv.currentIndex < currentOperand.opr.availabilityIndex && (countUnreadedOperands(_op)) > countLockedOperands(_conv, _op)) || currentOperand.isReaded){
                _i++;
                continue;
            }
            while (_conv.currentIndex < currentOperand.opr.availabilityIndex) {
                //System.out.println("System waiting for release operand: " + _op.get(_i).opr.getName());
                _conv.getBlock(stageSelectionOperandsAdditional).setCommand(_cmd, " - чт. " + (!currentOperand.type.equals("FLAG") ? currentOperand.opr.getName() : "FLAGS"), _conv.currentIndex++);
            }
            //константа
            if (currentOperand.opr.isConstant()) {
                numberOfTakts = isSameTaktLength ? 3 : 1;
            }
            //регистр или флаг
            else if (indexInRegister(currentOperand.opr.getName()) != -1 || indexInFlags(currentOperand.opr) != -1) {
                numberOfTakts = isSameTaktLength ? 3 : 1;
            }
            //кэш-промах
            else if (!missingCash && currentOperand.opr.isInternalMemory() && indexInCashMemory(currentOperand.opr) == -1) {
                numberOfTakts = isSameTaktLength ? 3 : 10;
                missingCash = !missingCash;
            }
            //кэш-попадание
            else {
                numberOfTakts = 3;
                cashMemory.add(new CashLine(_i, currentOperand.opr));
            }
            for (int _x = 0; _x < numberOfTakts; _x++) {
                if (currentOperand.type.equals("FLAG") && registerFlagReaded) {
                    continue;
                } else {
                    _conv.getBlock(stageSelectionOperandsAdditional).setCommand(_cmd, " - чт. " + (!currentOperand.type.equals("FLAG") ? currentOperand.opr.getName() : "FLAGS"), _conv.currentIndex++);
                }
            }
            currentOperand.isReaded = true;
            if (currentOperand.type.equals("FLAG")) {
                registerFlagReaded = true;
            }
            _i++;
        }
        while (!_conv.getBlock(stageExecution).isEmptyLine(_conv.currentIndex) || _conv.currentIndex < _conv.getLastIndex(stageExecution)) {
            if (currentOperand != null) {
                _conv.getBlock(stageSelectionOperandsAdditional).setCommand(_cmd, " - чт. " + (!currentOperand.type.equals("FLAG") ? currentOperand.opr.getName() : "FLAGS"), _conv.currentIndex++);
                currentOperand.isReaded = true;
                _i++;
            }
            else {
                _conv.currentIndex++;
            }
        }
        _op = null;
        processStageFour(_conv, _cmd);
    }

    //выполнение команды
    private void processStageFour(Conveyor _conv, Command _cmd){
        //System.out.println("CMD: " + _cmd + " STAGE 4");
        if (_cmd.isEmptyCommand()) {
            //System.out.println("EMPTY CMD: " + _cmd + " STAGE 4");
            processStageFive(_conv, _cmd);
            return;
        }
//        System.out.println("EMPTY CMD: " + _cmd + " STAGE 4");
        numberOfTakts = isSameTaktLength ? 3 : operationTaktMap.get(_cmd.getName());
        if (operationTaktMap.get(_cmd.getName()) == 0){
            numberOfTakts = 0;
        }
        for (int _i = 0; _i < numberOfTakts; _i++) {
            //System.out.println("Current INDEX: " + _conv.currentIndex);
            _conv.getBlock(stageExecution).setCommand(_cmd, "", _conv.currentIndex++);
        }
        while (!_conv.getBlock(stageWriting).isEmptyLine(_conv.currentIndex)) {
            if (numberOfTakts != 0) _conv.getBlock(stageExecution).setCommand(_cmd, "", _conv.currentIndex++);
            else _conv.currentIndex++;
        }
        //цикл REPE (итерации не закончены) - повторение стадий 3-4
        if (_cmd.isRepeCommand() && currentIteration < numberOfIterations){
            currentIteration++;
            processStageThree(_conv, _cmd);
        }
        else {
            processStageFive(_conv, _cmd);
        }
    }

    //запись результатов
    private void processStageFive(Conveyor _conv, Command _cmd){
        if (_cmd.isEmptyCommand()){
            return;
        }
        //System.out.println("CMD: " + _cmd + " == ZF = " + getFlag("ZF").value);
        numberOfTakts = isSameTaktLength ? 3 : 1;
        for (int _i = 0; _i < numberOfTakts; _i++) {
            _conv.getBlock(stageWriting).setCommand(_cmd, "", _conv.currentIndex++);
        }
        for (Flag _op: _cmd.getAllOperands()){
            //if (_op.accessMode.equals("w")) {
            _op.opr.availabilityIndex = _conv.currentIndex;
            if (_op.opr.getName().equals("AL")){
                if (getOperand("AX") != null){
                    getOperand("AX").availabilityIndex = _conv.currentIndex;
                }
            }
            //System.out.println("STAGE 5: CMD " + _cmd + " OPR: " + _op.opr + " AVAILABILITY INDEX: " + _op.opr.availabilityIndex);
            //}
        }
        //переход по метке - цикл
        if (_cmd.isLoopCommand()){
            //System.out.println("STAGE 5: CMD " + _cmd + "  OPR IS LOOP");
            if (currentIteration < numberOfIterations) {
                String gotoLabel = _cmd.getOperand(0).opr.getName();
                iter = pkM.get(gotoLabel).listIterator();
                keyIndex = keys.indexOf(gotoLabel);
                currentIteration++;
                if (!isPredictionAdded) {
                    _conv.startIndex = _conv.currentIndex;

                }
            }
            else {
                currentIteration = 0;
            }
        }
        if (_cmd.isJNCommand() || _cmd.isJCXZCommand()){
            if (getFlag("ZF").value == 0 && xPath < 2){
                String gotoLabel = _cmd.getOperand(0).opr.getName();
                iter = pkM.get(gotoLabel).listIterator();
                keyIndex = keys.indexOf(gotoLabel);
                getFlag("ZF").value = 1;
                xPath++;
            }
            else{
                getFlag("ZF").value = 0;
                xPath++;
            }
            //предсказание переходов - если нет, то ждем, пока выполнится команда условного перехода
            if (!isPredictionAdded) {
                _conv.startIndex = _conv.currentIndex;

            }
        }
        //условый переход по метке - goto
        else if (_cmd.isJumpCommand()) {
            //System.out.println("STAGE 5:  CMD " + _cmd + "  CONDITIONAL");
            if (getFlag("ZF").value == 1 && xPath < 2) {
                    //переход по метке, если ZF == 1
                String gotoLabel = _cmd.getOperand(0).opr.getName();
                iter = pkM.get(gotoLabel).listIterator();
                keyIndex = keys.indexOf(gotoLabel);
                getFlag("ZF").value = 0;
                xPath++;
            }
            else{
                getFlag("ZF").value = 1;
                xPath++;
            }
            //предсказание переходов - если нет, то ждем, пока выполнится команда условного перехода
            if (!isPredictionAdded) {
                _conv.startIndex = _conv.currentIndex;

            }
        }
        //бзусловый переход по метке - goto
        else if (_cmd.isJMPCommand()) {
            String gotoLabel = _cmd.getOperand(0).opr.getName();
            iter = pkM.get(gotoLabel).listIterator();
            keyIndex = keys.indexOf(gotoLabel);
            if (!isPredictionAdded) {
                _conv.startIndex = _conv.currentIndex;
            }
        }
        //вызов процедуры
        else if (_cmd.isCallFunctionCommand()){
            //System.out.println("STAGE 5:  CMD " + _cmd + "  CALL PROCEDURE");
            String gotoLabel = _cmd.getOperand(0).opr.getName();
            callProcedureIter = iter;
            iter = procedureList.get(gotoLabel).listIterator();
        }
        //возврат из процедуры
        else if (_cmd.isReturnCommand()){
            //callProcedureIter.previous();
            iter = callProcedureIter;
        }
    }

    //подготовка к выборке
    private void processStagePrepareSelection(Conveyor _conv, Command _cmd){
        _conv.currentIndex = _conv.startIndex;
        numberOfTakts = isSameTaktLength ? 3 : 1;
        //boolean added = false;
        for (int _i = 0; _i < numberOfTakts; _i++) {
            _conv.getBlock(stagePrepareSelectionCommand).setCommand(_cmd, "", _conv.currentIndex++);
        }
        while (!_conv.getBlock(stageEncoding).isEmptyLine(_conv.currentIndex)) {
            _conv.getBlock(stagePrepareSelectionCommand).setCommand(_cmd, "", _conv.currentIndex++);
            //added = true;
        }
        _conv.startIndex = _conv.currentIndex;
        processStageEncoding(_conv, _cmd);
    }

    private void processStageEncoding(Conveyor _conv, Command _cmd){
        boolean added = false;
        numberOfTakts = isSameTaktLength ? 3 : 1;
        for (int _i = 0; _i < numberOfTakts; _i++) {
            _conv.getBlock(stageEncoding).setCommand(_cmd, "", _conv.currentIndex++);
        }
        while (!_conv.getBlock(stageSelectionCommand).isEmptyLine(_conv.currentIndex)) {
            _conv.getBlock(stageEncoding).setCommand(_cmd, "", _conv.currentIndex++);
            added = true;
        }
        processStageOne(_conv, _cmd);
    }

    private int indexInCashMemory(Operand _op){
        for (int _i = 0; _i < cashMemory.size(); _i++){
            if (cashMemory.get(_i).getOperand() == _op){
                return _i;
            }
        }
        return -1;
    }

    private int indexInRegister(String _op){
        for (int _i = 0; _i < pullRegister.size(); _i++){
            if (pullRegister.get(_i).getName().equals(_op)){
                return _i;
            }
        }
        return -1;
    }

    private int indexInFlags(Operand _op){
        for (int _i = 0; _i < pullFlags.size(); _i++){
            if (pullFlags.get(_i) == _op){
                return _i;
            }
        }
        return -1;
    }

    private Operand getFlag(String _name){
        for (int i = 0; i < pullFlags.size(); i++){
            if (pullFlags.get(i).equals(_name)){
                return pullFlags.get(i);
            }
        }
        return null;
    }

    private Operand getOperand(String _name){
        for (int i = 0; i < pullOperands.size(); i++){
            if (pullOperands.get(i).equals(_name)){
                return pullOperands.get(i);
            }
        }
        return null;
    }

    //подсчет считаных операндов команды
    private int countReadedOperands(ArrayList<Flag> _operands){
        int result = 0;
        for (Flag operand: _operands){
            if (operand.isReaded){
                result++;
            }
        }
        return result;
    }

    //подсчет несчитаных операндов команды
    private int countUnreadedOperands(ArrayList<Flag> _operands){
        int result = 0;
        boolean flag = false;
        for (Flag operand: _operands){
            if (!operand.isReaded){
                //if (!flag) result++;
                result++;
                //if (operand.type.equals("FLAG")) flag=true; //флаг учитывается только 1 раз
            }
        }
        return result;
    }

    private int countLockedOperands(Conveyor _conv, ArrayList<Flag> _operands){
        int result = 0;
        for (Flag operand: _operands){
            if (_conv.currentIndex <= operand.opr.availabilityIndex){
                result++;
            }
        }
        return result;
    }

    //установка параметров работы конвейера
    public void setParameters(Parameters param){

        isPredictionAdded = param.addPredict;
        isAdditionalStagesAdded = param.splitFirst;
        isDiscardCashMemory = param.discardCash;
        isSameTaktLength = param.sameTakt;
        isReuseProhibited = param.reuseProhibited;
        isDuplicateStage = param.duplicateStage;
        numOfConveyors = param.convNum;
        variant = param.varNum;
        instructions = param.ins;
        numOfBlocks = 5;
        if (isAdditionalStagesAdded) {
            numOfBlocks += 2;
            stagePrepareSelectionCommand = 0;
            stageEncoding = 1;
            stageSelectionCommand += 2;
            stageDecodingCommand += 2;
            stageSelectionOperands += 2;
            stageExecution += 2;
            stageWriting += 2;
        }
        if (isDuplicateStage){
            numOfBlocks += 1;
            stageSelectionOperandsAdditional = stageSelectionOperands + 1;
            stageExecution += 1;
            stageWriting += 1;
        }

        paramSet = true;

        conveyors.clear();

        numberOfIterations = (variant == 2 || variant == 9) ? 0 : 3;

        for (int _i = 0; _i < numOfConveyors; _i++){
            conveyors.add(new Conveyor(numOfBlocks));
        }

        loadInstructions();
    }

    public boolean isParametersSetted(){ return paramSet; }

    public boolean isTaskDone() {  return conveyors.size() > 0 && conveyors.get(0).size() > 0; }

    //считывание команд и операндов
    public void loadInstructions(){
        try {
            initDefaultOperandsAndFlags();
            String[] lines = instructions.split("\n");
            ArrayList<Command> pullCommandRef = pkM.get("DEFAULT");
            for (String _line : lines) {
                int commentIndex = _line.indexOf(';');
                String[] attributes = (commentIndex != -1) ? _line.substring(0, commentIndex).split(" ") : _line.split(" ");
                Command cmd;
                if (attributes[0].contains(":")){
                    cmd = new Command(attributes[0].split(":")[1].toUpperCase().trim(), operationTaktMap.get(attributes[0].split(":")[1].toUpperCase().trim()));
                    cmd.setGotoLabel(new Operand(attributes[0].split(":")[0].toUpperCase().trim()));
                    //процедуры записываются в отдельный пул
                    if (cmd.isProcedureCommand()){
                        procedureList.put(cmd.getGotoLabel().getName(), new ArrayList<>());
                        pullCommandRef = procedureList.get(cmd.getGotoLabel().getName());
                    }
                    else {
                        pkM.put(cmd.getGotoLabel().getName(), new ArrayList<>());
                        keys.add(cmd.getGotoLabel().getName());
                        pullCommandRef = pkM.get(cmd.getGotoLabel().getName());
                    }
                }
                else{
                    cmd = new Command(attributes[0].toUpperCase().trim(), operationTaktMap.get(attributes[0].toUpperCase().trim()));
                    //команда выхода из процедуры - запись продолжается в крайний пул
                }
                if (commentIndex != -1) {
                    cmd.setComment(_line.substring(commentIndex, _line.length()).replace(";", " ").trim());
                }
                if (attributes.length > 1) {
                    String[] operands = attributes[1].split(",");
                    int _i = 0;
                    for (String _op : operands) {
                        //System.out.println(_op);
                        int index = indexOperandInPull(_op); //проверка на наличие операнда в памяти
                        if (index == -1) { //операнд еще не добавлен
                            pullOperands.add(new Operand(_op));
                            if (cmd.isConditionalCommand()){
                                pullOperands.get(pullOperands.size() - 1).setAddressType(AddressType.CONSTANT);
                            }
                            //cmd.addOperand(pullOperands.get(pullOperands.size() - 1));
                            cmd.addOperand(pullOperands.get(pullOperands.size() - 1));
                        }
                        else{  //операнд уже в памяти
                            //cmd.addOperand(pullOperands.get(index));
                            cmd.addOperand(pullOperands.get(index));
                        }
                        if (_i==0 && !cmd.noResult()){
                            cmd.getOperand(cmd.getOperands().size() - 1).accessMode = "w";
                        }
                        if ((cmd.isMoveCommand() || cmd.isLea()) && _i==0){
                            //cmd.removeOperand();
                            cmd.getOperand(cmd.getOperands().size() - 1).isReaded = true;
                        }
                        _i++;
                    }
                }
                //добавление неявных операндов
                if (cmd.isLoopCommand()){
                    cmd.addRegister(pullOperands.get(indexOperandInPull("ECX")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("IP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                }
                else if (cmd.isXORCommand()){
                    cmd.addFlag(getFlag("SF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("AF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("CF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isJMPCommand()){
                    cmd.addRegister(pullOperands.get(indexOperandInPull("IP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                }
                else if (cmd.isJNCommand() || cmd.isJCXZCommand()){
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("IP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                }
                else if (cmd.isJumpCommand()){
                    //cmd.addOperand(getFlag("ZF"));
                    //cmd.addOperand(pullOperands.get(indexOperandInPull("ESI")));
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("IP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                }
                else if (cmd.isAryphSimple()){
                    //cmd.addOperand(getFlag("SF"));
                    //cmd.addOperand(getFlag("AF"));
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("SF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("AF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isAndCommand()){
                    cmd.addFlag(getFlag("AF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isCLCCommand()){
                    cmd.addFlag(getFlag("CF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isAryphCarried()){
                    cmd.addFlag(getFlag("SF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("AF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("CF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isCallFunctionCommand()){
                    cmd.addRegister(pullOperands.get(indexOperandInPull("IP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("SP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                }
                else if (cmd.isReturnCommand()){
                    cmd.addRegister(pullOperands.get(indexOperandInPull("IP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("SP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    addOperand("[MEM+ESI]", cmd);
                    cmd.getOperand(cmd.getOperands().size() - 1).isVisible = false;
                }
                else if (cmd.isCompareCommand()){
                    //cmd.addOperand(getFlag("ZF"));
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("PF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isDivision()){
                    //cmd.addOperand(pullOperands.get(indexOperandInPull("EDX")));
                    //cmd.addOperand(pullOperands.get(indexOperandInPull("EAX")));
                    cmd.addRegister(pullOperands.get(indexOperandInPull("EAX")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("EDX")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                }
                else if (cmd.isMultiply()){
                    cmd.addRegister(pullOperands.get(indexOperandInPull("AX")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    if (!cmd.hasOperand("DX")){
                        cmd.addRegister(pullOperands.get(indexOperandInPull("DX")));
                        cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    }
                }
                else if (cmd.isStackControl()){
                    //cmd.addOperand(pullOperands.get(indexOperandInPull("ESP")));
                    cmd.addRegister(pullOperands.get(indexOperandInPull("ESP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    //cmd.addRegister(pullOperands.get(indexOperandInPull("AX")));
                    //cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    addOperand("AX", cmd);
                    cmd.getOperand(cmd.getOperands().size() - 1).isVisible = false;
                    cmd.getOperand(cmd.getOperands().size() - 1).isReaded = true;
                }
                else if (cmd.isStackControlFlags()){
                    //cmd.addOperand(pullOperands.get(indexOperandInPull("ESP")));
                    //cmd.addOperand(getFlag("PF"));
                    cmd.addRegister(pullOperands.get(indexOperandInPull("ESP")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("PF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isControlDF()){
                    //cmd.addOperand(getFlag("DF"));
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isORCommand()){
                    //cmd.addOperand(getFlag("DF"));
                    cmd.addFlag(getFlag("PF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isRepeCommand()){
                    cmd.getOperand(cmd.getOperands().size() - 1).isReaded = true;
                    cmd.addRegister(pullOperands.get(indexOperandInPull("ESI")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addRegister(pullOperands.get(indexOperandInPull("EDI")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    addOperand("[MEM1]", cmd);
                    cmd.getOperand(cmd.getOperands().size() - 1).isVisible = false;
                    addOperand("[MEM2]", cmd);
                    cmd.getOperand(cmd.getOperands().size() - 1).isVisible = false;
                    cmd.addRegister(pullOperands.get(indexOperandInPull("ECX")));
                    cmd.getRegister(cmd.getRegisters().size() - 1).accessMode = "w";
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isProcedureCommand()){
                    cmd.getOperand(cmd.getOperands().size() - 1).isReaded = true;
                }
                else if (cmd.isShiftCommand()){
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                else if (cmd.isIncDecCommand()){
                    cmd.addFlag(getFlag("ZF"));
                    cmd.getFlag(cmd.getFlags().size() - 1).accessMode = "w";
                }
                pullCommandRef.add(cmd);
                if (cmd.getName().equals("RET")){
                    pullCommandRef = pkM.get(keys.get(keys.size() - 1));
                    //cmd.getOperand(cmd.getOperands().size() - 1).isReaded = true;
                }
            }
            pullCommandRef = null;
            ///////////////////////////
        }
        catch (Exception ex){
            System.out.println("Error: " + ex.getMessage() + "\n" + "Cause: " + ex.getCause());
            ex.printStackTrace();
        }
    }

    private void addOperand(String _operand, Command _cmd){
        int index = indexOperandInPull(_operand);
        if (index == -1){
            pullOperands.add(new Operand(_operand));
            _cmd.addOperand(pullOperands.get(pullOperands.size() - 1));
        }
        else{
            _cmd.addOperand(pullOperands.get(index));
        }
    }

    private void initDefaultOperandsAndFlags(){
        pkM.put("DEFAULT", new ArrayList<>());
        keys.add("DEFAULT");
        pullFlags.clear();
        pullOperands.clear();

        //регистры
        pullOperands.add(new Operand("AX"));
        pullOperands.add(new Operand("DX"));
        pullOperands.add(new Operand("CX"));
        pullOperands.add(new Operand("BX"));
        pullOperands.add(new Operand("AL"));
        pullOperands.add(new Operand("DX"));
        pullOperands.add(new Operand("EAX"));
        pullOperands.add(new Operand("EDX"));
        pullOperands.add(new Operand("EDI"));
        pullOperands.add(new Operand("ESI"));
        pullOperands.add(new Operand("EIP"));
        pullOperands.add(new Operand("ESP"));
        pullOperands.add(new Operand("IP"));
        pullOperands.add(new Operand("SP"));

        for (int _i = 0; _i < pullOperands.size(); _i++) {
            pullRegister.add(pullOperands.get(_i));
        }

        int _startIndex = pullOperands.size();

        //флаги
        pullOperands.add(new Operand("ZF"));
        //арифметические флаги
        pullOperands.add(new Operand("CF"));
        pullOperands.add(new Operand("SF"));
        pullOperands.add(new Operand("AF"));
        //флаг строк
        pullOperands.add(new Operand("DF"));
        //флаг паритета
        pullOperands.add(new Operand("PF"));

        for (int _i = _startIndex; _i < pullOperands.size(); _i++){
            pullFlags.add(pullOperands.get(_i));
        }
    }

    //поиск операнда в пуле и возврат его индекса (-1 если не найден)
    private int indexOperandInPull(String _operandName){
        try{
            for (int _i = 0; _i < pullOperands.size(); _i++){
                if (pullOperands.get(_i).equals(_operandName)){
                    return _i;
                }
            }
            return -1;
        }
        catch (Exception ex){
            System.out.println("Error: " + ex.getMessage() + "\n" + "Cause: " + ex.getCause());
            return -1;
        }
    }

    public String hasAdditionalStages(){
        return isAdditionalStagesAdded ? "Y" : "N";
    }

    public String hasPrediction() {
        return isPredictionAdded ? "Y" : "N";
    }

    public String hasSameLength() {
        return isSameTaktLength ? "Y" : "N";
    }

    public String hasDiscardCash() {
        return isDiscardCashMemory ? "Y" : "N";
    }

    public String hasProhibitedReuse() {return isReuseProhibited ? "Y" : "N"; }

    public String hasDuplicateStage() { return isDuplicateStage ? "Y" : "N"; }
}
